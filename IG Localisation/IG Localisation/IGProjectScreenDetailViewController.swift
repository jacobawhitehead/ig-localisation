//
//  IGProjectScreenDetailViewController.swift
//  IG Localisation
//
//  Created by Jacob Whitehead on 21/07/2016.
//  Copyright © 2016 Level Headed Studio. All rights reserved.
//

import UIKit

class IGProjectScreenDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, IGLocalisationControllerDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var numberOfTranslationsLabel: UILabel!
    
    var project:IGProjectScreenModel!
    var localisationController:IGLocalisationController = IGLocalisationController()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = self.project.title
        self.localisationController.delegate = self
        self.fetchListOfLocalisations()
        
        let nib = UINib(nibName: "IGLocalisationTableViewCell", bundle: nil)
        self.tableView.registerNib(nib, forCellReuseIdentifier: "localisationCell")
        
        self.tableView.separatorStyle = .None
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 140
    }
    
    private func reloadWithAnimation() {
        let indexSet = NSIndexSet(index: 0)
        self.tableView.reloadSections(indexSet, withRowAnimation: .Fade)
    }
    
    func fetchListOfLocalisations() {
        for localisationID in self.project.localisationListID {
            self.localisationController.fetchLocalisationListForID(localisationID)
        }  
    }
    
    //MARK - TableViewDelegate
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let labelText = String(self.localisationController.localisationList.count) + " translations"
        self.numberOfTranslationsLabel.text = labelText
        return self.localisationController.localisationList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("localisationCell") as! IGLocalisationTableViewCell
        cell.titleLabel.text = self.localisationController.localisationList[indexPath.row].text
         return cell
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let localisation = self.localisationController.localisationList[indexPath.row]
        
        let localisationDetailVC = self.storyboard?.instantiateViewControllerWithIdentifier("localisationDetailViewController") as! IGLocalisationDetailViewController
        localisationDetailVC.localisation = localisation
        
        self.navigationController?.pushViewController(localisationDetailVC, animated: true)
    }
    
    //MARK - LocalisationControllerDelegate
    func localisationControllerListDidUpdate() {
        self.reloadWithAnimation()
    }
}
