//
//  IGLocaleList.swift
//  IG Localisation
//
//  Created by Jacob Whitehead on 25/07/2016.
//  Copyright © 2016 Level Headed Studio. All rights reserved.
//

let kLocaleFrench = "fr_FR"
let kLocaleEnglish = "en_GB"
let kLocaleGerman = "de_DE"
let kLocaleItalian  = "it_IT"

