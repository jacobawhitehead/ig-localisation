//
//  IGLoginViewController.swift
//  IG Localisation
//
//  Created by Jacob Whitehead on 20/07/2016.
//  Copyright © 2016 Level Headed Studio. All rights reserved.
//

import UIKit
import Firebase

class IGLoginViewController: UIViewController, IGProjectsProtocol {
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var statusLabel: UILabel!
    
    //reference to the next page (projects) so we can set it up after login
    var projectsViewContoller:IGProjectsViewController!
    var createUserViewController:UINavigationController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        self.projectsViewContoller = storyboard.instantiateViewControllerWithIdentifier("projectsViewController") as! IGProjectsViewController
        self.projectsViewContoller.delegate = self
        
        self.createUserViewController = storyboard.instantiateViewControllerWithIdentifier("createUserViewController") as! UINavigationController
        
        self.scrollView.scrollEnabled = false
        self.statusLabel.alpha = 0.0
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(IGLoginViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(IGLoginViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(IGLoginViewController.viewTapped))
        self.view.addGestureRecognizer(tap)
        
        self.activityIndicator.hidden = true;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func loginButtonPressed(sender: AnyObject) {
        let email = self.emailTextfield.text!
        let password = self.passwordTextfield.text!
        
        self.view.endEditing(true)
        
        self.showLoadingIndicator()
        
        self.loginWithEmail(email, password: password)
    }
    
    func loginWithEmail(email:String, password:String) {
        self.changeStatusTo("Logging in")
        self.statusLabel.textColor = UIColor.lightGrayColor()
        FIRAuth.auth()?.signInWithEmail(email, password: password) { (user, error) in
            if (error == nil) {
                self.didSuccessfullyLogin()
            } else {
                if let firAuthError = FIRAuthErrorCode(rawValue: (error?.code)!) {
                    if (firAuthError == FIRAuthErrorCode.ErrorCodeUserNotFound) {
                        self.createUserWithEmail(email, password: password)
                    } else {
                        self.handleLoginError(error!)
                        self.hideLoadingIndicator()
                    }
                }
            }
        }
    }
    
    func createUserWithEmail(email:String, password:String) {
        self.changeStatusTo("Creating account")
        FIRAuth.auth()?.createUserWithEmail(email, password: password) { (user, error) in
            if (error == nil) {
                self.didSuccessfullyCreateUser()
            } else {
                self.handleLoginError(error!)
                self.hideLoadingIndicator()
            }
        }
    }
    
    private func didSuccessfullyLogin() {
        self.changeStatusTo("Logged in")
        self.projectsViewContoller.getProjects()
        self.changeStatusTo("Loading data")
    }
    
    private func didSuccessfullyCreateUser() {
        self.presentViewController(self.createUserViewController, animated: true, completion: nil)
    }
    
    private func showLoadingIndicator() {
        self.activityIndicator.hidden = false;
        self.activityIndicator.startAnimating()
        
        UIView.animateWithDuration(0.2) {
            self.heightConstraint.constant = 0
            self.loginButton.alpha = 0.0
            self.containerView.alpha = 0.0
            self.view.layoutIfNeeded()
        }
    }
    
    private func hideLoadingIndicator() {
        self.activityIndicator.hidden = true;
        self.activityIndicator.stopAnimating()
        
        UIView.animateWithDuration(0.2) {
            self.heightConstraint.constant = 180.0
            self.loginButton.alpha = 1.0
            self.containerView.alpha = 1.0
            self.view.layoutIfNeeded()
        }
    }
    
    @objc private func viewTapped() {
        self.view.endEditing(true)
    }
    
    private func changeStatusTo(status:String) {
        self.statusLabel.alpha = 1.0
        UIView.animateWithDuration(0.2, delay: 0.5, options:.CurveEaseIn, animations: {
                self.statusLabel.text = status
            }) { (completed) in
                
        }
    }
    
    private func handleLoginError(error: NSError) {
        self.statusLabel.textColor = UIColor.redColor()
        if let authError = FIRAuthErrorCode(rawValue: error.code) {
            switch authError {
            case FIRAuthErrorCode.ErrorCodeInvalidEmail:
                self.statusLabel.text = "Invalid email"
            default:
                self.statusLabel.text = "Error"
            }
        }
    }
    
    //MARK - Keyboard
    func keyboardWillShow(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        
        let contentInsets = UIEdgeInsetsMake(0, 0, keyboardFrame.height, 0)
        scrollView.contentInset = contentInsets;
        scrollView.scrollIndicatorInsets = contentInsets;
        
        let offset = -(self.passwordTextfield.frame.origin.y - keyboardFrame.height)
        scrollView.setContentOffset(CGPointMake(0, offset), animated: true)
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        scrollView.contentInset = UIEdgeInsetsZero;
        scrollView.scrollIndicatorInsets = UIEdgeInsetsZero;
    }
    
    //MARK - IGProjectsProtocol
    func didFinishLoadingProjects() {
        let navigationController = UINavigationController(rootViewController: self.projectsViewContoller)
        navigationController.modalTransitionStyle = .CrossDissolve
        self.presentViewController(navigationController, animated: true, completion: nil)
    }

}

