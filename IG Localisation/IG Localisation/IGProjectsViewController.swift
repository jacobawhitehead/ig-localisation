//
//  IGProjectsViewController.swift
//  IG Localisation
//
//  Created by Jacob Whitehead on 20/07/2016.
//  Copyright © 2016 Level Headed Studio. All rights reserved.
//
import UIKit
import Firebase

protocol IGProjectsProtocol: class {
    func didFinishLoadingProjects()
}

struct IGProject {
    var title:String!
    var screens = [String]()
    
    mutating func initWithSnapshot(snapshot:FIRDataSnapshot) {
        self.title = snapshot.value!["title"] as? String ?? "ERROR"
        if let data = snapshot.value!["screens"] as? [String:Bool] {
            for (screenID, _) in data {
                self.screens.append(screenID)
            }
        }
    }
}

class IGProjectsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    weak var delegate:IGProjectsProtocol?
    
    @IBOutlet weak var tableView: UITableView!
    private var projects:[IGProject] = []
    private let ref = FIRDatabase.database().reference().child("project")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Projects"
        if (self.projects.count == 0) {
            self.getProjects()
        }
    }
    
    func getProjects() {
        self.ref.observeSingleEventOfType(.Value, withBlock: { (snapshot) in
            let enumerator = snapshot.children
            while let projectSnapshot = enumerator.nextObject() as? FIRDataSnapshot {
                var project = IGProject()
                project.initWithSnapshot(projectSnapshot)
                self.projects.append(project)
            }
            
            self.delegate?.didFinishLoadingProjects()
        }) { (error) in
                print(error.localizedDescription)
        }
    }
    
    @IBAction func addProjectButtonTapped(sender: AnyObject) {
        let addProjectAlert = UIAlertController(title: "Create project", message: "", preferredStyle: .Alert)
        addProjectAlert.addTextFieldWithConfigurationHandler { (textField:UITextField) in
            textField.placeholder = "Enter name here"
            textField.delegate = self
        }
    }
    
    //MARK - TextFieldDelegate
    func textFieldDidEndEditing(textField: UITextField) {
    }
    
    //MARK - TableViewDelegate
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.projects.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = self.projects[indexPath.row].title
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let project = self.projects[indexPath.row]

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let screensViewController = storyboard.instantiateViewControllerWithIdentifier("projectScreenViewController") as! IGProjectScreensViewController
        
        screensViewController.screens = project.screens
        
        self.navigationController?.pushViewController(screensViewController, animated: true)
    }
}
