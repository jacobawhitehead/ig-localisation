//
//  IGTranslationController.swift
//  IG Localisation
//
//  Created by Jacob Whitehead on 23/07/2016.
//  Copyright © 2016 Level Headed Studio. All rights reserved.
//

import Firebase

let kTranslationRef = "translation"

protocol TranslationControllerDelegate {
    func didFinishFetchingTranslations()
}

class IGTranslationController {
    var translationsDictionary = [String:String]()
    var delegate:TranslationControllerDelegate?
    
    func fetchTranslationForKey(key: String) {
        let ref = FIRDatabase.database().reference().child(kTranslationRef).child(key)
        let dict = NSMutableDictionary()
        ref.observeSingleEventOfType(.Value) { (snapshot:FIRDataSnapshot) in
            for child in snapshot.children {
                dict.setValue(child.value, forKey: child.key)
            }
            
            self.translationsDictionary = dict.copy() as! [String : String]
            self.delegate?.didFinishFetchingTranslations()
        }
    }
    
    func translationsArray() -> [String] {
        var array = [String]()
        for (_, value) in self.translationsDictionary {
            array.append(value)
        }
        
        return array
    }
    
    func translationCodeArray() -> [String] {
        var array = [String]()
        for (key, _) in self.translationsDictionary {
            array.append(key)
        }
        
        return array
    }
}
