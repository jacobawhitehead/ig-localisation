//
//  IGConstants.swift
//  IG Localisation
//
//  Created by Jacob Whitehead on 29/07/2016.
//  Copyright © 2016 Level Headed Studio. All rights reserved.
//

import Foundation

//platforms
let kPlatformIOS = "iOS"
let kPlatformAndroid = "Android"
let kPlatformWeb = "Web"
