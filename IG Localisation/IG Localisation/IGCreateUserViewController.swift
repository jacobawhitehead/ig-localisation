//
//  IGCreateUserViewController.swift
//  IG Localisation
//
//  Created by Jacob Whitehead on 25/07/2016.
//  Copyright © 2016 Level Headed Studio. All rights reserved.
//

import UIKit

class IGCreateUserViewController: UIViewController {
    
    @IBOutlet weak var addLanguageButton: UIButton!
    @IBOutlet weak var teamLabel: UILabel!
    @IBOutlet weak var teamPickerView: UIPickerView!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    var localeListViewController:IGLocaleListViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func doneButtonPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func addLanguageButtonPressed(sender: AnyObject) {
        //TODO: Do this with a segue and confrom to delegate
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        self.localeListViewController = storyboard.instantiateViewControllerWithIdentifier("localeListViewController") as! IGLocaleListViewController
        self.navigationController?.pushViewController(self.localeListViewController, animated: true)
    }
}
