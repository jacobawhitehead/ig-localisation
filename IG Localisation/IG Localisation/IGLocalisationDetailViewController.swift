//
//  IGLocalisationDetailViewController.swift
//  IG Localisation
//
//  Created by Jacob Whitehead on 23/07/2016.
//  Copyright © 2016 Level Headed Studio. All rights reserved.
//

import UIKit

class IGLocalisationDetailViewController: UIViewController, TranslationControllerDelegate {
    @IBOutlet weak var contextLabel: UILabel!
    @IBOutlet weak var translationContainerView: UIView!
    @IBOutlet weak var textLabel: UILabel!
    
    var localisation: IGLocalisation!
    var translationController = IGTranslationController()
    
    var translationPageViewController:IGTranslationPageViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.textLabel.text = localisation.text
        self.contextLabel.text = localisation.context
        
        self.translationController.delegate = self
        
        translationController.fetchTranslationForKey(localisation.translationID)
        
        self.translationPageViewController = self.storyboard!.instantiateViewControllerWithIdentifier("translationPageViewController") as! IGTranslationPageViewController
        self.displayContentForContainer(self.translationPageViewController)
    }
    
    func didFinishFetchingTranslations() {
        self.translationPageViewController.translations = self.translationController.translationsArray()
        self.translationPageViewController.translationCodes = self.translationController.translationCodeArray()
        self.translationPageViewController.setup()
    }
    
    func displayContentForContainer(content: UIViewController) {
        self.addChildViewController(content)
        content.view.frame = self.translationContainerView.frame
        self.translationContainerView.addSubview(content.view)
        content.didMoveToParentViewController(self)
    }
}
