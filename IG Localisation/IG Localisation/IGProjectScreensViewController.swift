//
//  IGProjectScreensViewController.swift
//  IG Localisation
//
//  Created by Jacob Whitehead on 28/07/2016.
//  Copyright © 2016 Level Headed Studio. All rights reserved.
//

import UIKit

enum IGTeam: Int {
    case iOS = 0, Android, Web
}

class IGProjectScreensViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, IGProjectScreenDelegate{
    
    var screens = [String]()

    @IBOutlet weak private var teamSegmentedControl: UISegmentedControl!
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var screenCountLabel: UILabel!
    
    private lazy var screensController:IGProjectScreensController = {
        let controller = IGProjectScreensController(screenIDs: self.screens)
        controller.delegate = self
        return controller
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screensController.getScreens()
    }
    
    @IBAction func segmentedControlChanged(sender: AnyObject) {
        self.reloadWithAnimation()
    }
    
    private func reloadWithAnimation() {
        let indexSet = NSIndexSet(index: 0)
        self.tableView.reloadSections(indexSet, withRowAnimation: .Fade)
    }
    
    //MARK - TableViewDelegate
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.returnListForCurrentlySelectedTeam().count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = self.screensController.iOSScreens[indexPath.row].title
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let project = self.returnListForCurrentlySelectedTeam()[indexPath.row]
        
        let screenDetailVC = self.storyboard?.instantiateViewControllerWithIdentifier("screenDetailViewController") as! IGProjectScreenDetailViewController
        screenDetailVC.project = project
        
        self.navigationController?.pushViewController(screenDetailVC, animated: true)
    }
    
    func returnListForCurrentlySelectedTeam() -> [IGProjectScreenModel] {
        var list: [IGProjectScreenModel] = []
        
        let selected = self.teamSegmentedControl.selectedSegmentIndex
        if let team = IGTeam(rawValue: selected) {
            switch team {
            case .iOS:
                list = self.screensController.iOSScreens
            case .Android:
                list = self.screensController.androidScreens
            case .Web:
                list = self.screensController.webScreens
            }
        }
        return list
    }
    
    //MARK - ProjectScreenDelegate
    func screenControllerDidUpdate() {
        self.reloadWithAnimation()
    }


}
