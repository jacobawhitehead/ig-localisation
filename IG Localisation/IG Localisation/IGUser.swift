//
//  IGUser.swift
//  IG Localisation
//
//  Created by Jacob Whitehead on 20/07/2016.
//  Copyright © 2016 Level Headed Studio. All rights reserved.
//

enum IGUserTitle {
    case IGDeveloper
    case IGTranslator
    case IGProductOwner
}

class IGUser {
    var id:String!
    var name:String!
    var email:String!
    var title:IGUserTitle!
}
