//
//  IGTranslationViewController.swift
//  IG Localisation
//
//  Created by Jacob Whitehead on 27/07/2016.
//  Copyright © 2016 Level Headed Studio. All rights reserved.
//

import UIKit

class IGTranslationViewController: UIViewController {

    var translation = ""
    var translationCode = ""
    
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet private weak var translationLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.translationLabel.text = translation
        self.languageLabel.text = translationCode
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
