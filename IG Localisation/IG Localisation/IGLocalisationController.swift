//
//  IGLocalisationStructs.swift
//  IG Localisation
//
//  Created by Jacob Whitehead on 21/07/2016.
//  Copyright © 2016 Level Headed Studio. All rights reserved.
//
import Firebase

let kLocalisationRef = "localisation"
let kNotifyLocalisationsLoadedEvent = "localisations.loaded.event"

protocol IGLocalisationControllerDelegate {
    func localisationControllerListDidUpdate()
}

//TODO: Split up these classes into seperate files (models/controllers)

class IGLocalisation {
    var text:String! = "Empty"
    var context:String! = "Empty"
    var translationID:String!
    var commentListID:String?
    
    func setupWithLocalisationId(snapshot: FIRDataSnapshot) {
        self.text = snapshot.value!["text"] as! String
        self.context = snapshot.value!["context"] as! String
        self.translationID = snapshot.value!["translation"] as! String
        self.commentListID = snapshot.value!["comment-list"] as? String ?? ""
    }
}


class IGLocalisationController {
    let ref = FIRDatabase.database().reference()
    var localisationList = [IGLocalisation]()
    
    var delegate:IGLocalisationControllerDelegate?
    
    func fetchLocalisationListForID(listID:String) {
        let listRef = self.ref.child(kLocalisationRef).child(listID)
        
        listRef.observeSingleEventOfType(.Value, withBlock: { (snapshot) in
            self.parseSnapshot(snapshot)
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    private func parseSnapshot(snapshot:FIRDataSnapshot) {
        let newLocalisation = IGLocalisation()
        newLocalisation.setupWithLocalisationId(snapshot)
        self.localisationList.append(newLocalisation)
        self.delegate?.localisationControllerListDidUpdate()
    }
}
