//
//  IGTranslationViewController.swift
//  IG Localisation
//
//  Created by Jacob Whitehead on 23/07/2016.
//  Copyright © 2016 Level Headed Studio. All rights reserved.
//

import UIKit

class IGTranslationPageViewController: UIPageViewController, UIPageViewControllerDataSource {
    
    var translations = [String]()
    var translationCodes = [String]()
    
    private var currentPageCode = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
    }
    
    func setup() {
        let firstTranslatedViewController = self.pageForCurrentIndex()

        firstTranslatedViewController.translation = self.translations[self.currentPageCode]
        self.setViewControllers([firstTranslatedViewController], direction: .Forward, animated: true, completion: nil)
    }
    
    private func pageForCurrentIndex() -> IGTranslationViewController {
        let page = self.storyboard!.instantiateViewControllerWithIdentifier("translationPage") as! IGTranslationViewController
        page.translation = self.translations[self.currentPageCode]
        page.translationCode = self.translationCodes[self.currentPageCode]
        return page
    }
    
    //MARK - PageViewControllerDataSource
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        if ((self.currentPageCode - 1) < 0) {
            return nil
        } else {
            self.currentPageCode = self.currentPageCode - 1
            return self.pageForCurrentIndex()
        }
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        if (self.currentPageCode + 1 == self.translations.count) {
            return nil
        } else {
            self.currentPageCode = self.currentPageCode + 1
            return self.pageForCurrentIndex()
        }
    }

}
