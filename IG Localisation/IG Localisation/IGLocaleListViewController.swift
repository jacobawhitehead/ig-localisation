//
//  IGLocaleListViewController.swift
//  IG Localisation
//
//  Created by Jacob Whitehead on 25/07/2016.
//  Copyright © 2016 Level Headed Studio. All rights reserved.
//

import UIKit
import Foundation

protocol IGLocaleListSelectedProtocol {
    func didFinishSelectingLanguages(languages:[String])
}

class IGLocaleListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var delegate:IGLocaleListSelectedProtocol?
    
    @IBOutlet weak var tableView: UITableView!
    var localeList: [String:String]!
    var languages: [String]! = []
    
    var selectedLanguages: [String]! = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.parseLocaleListFromPlist()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        if selectedLanguages.count != 0 {
            self.delegate?.didFinishSelectingLanguages(self.selectedLanguages)
        }
    }
    
    func parseLocaleListFromPlist() {
        let path = NSBundle.mainBundle().pathForResource("locales", ofType: "plist")
        let localesDicr = NSDictionary(contentsOfFile: path!)
        
        self.localeList = localesDicr?.objectForKey("locales") as! [String:String]
        
        for locale in localeList.values {
            self.languages.append(locale)
        }
    }
    
    //MARK - Table View Delegate
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.localeList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = self.languages[indexPath.row]
        cell.accessoryType = .None
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        if cell?.accessoryType.rawValue == 0 {
            cell?.accessoryType = .Checkmark
            self.selectedLanguages.append(self.languages[indexPath.row])
        } else {
            cell?.accessoryType = .None
        }
        
    }
}
