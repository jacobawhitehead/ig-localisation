//
//  IGSessionManager.swift
//  IG Localisation
//
//  Created by Jacob Whitehead on 20/07/2016.
//  Copyright © 2016 Level Headed Studio. All rights reserved.
//

import Foundation
import Firebase

class IGSessionManager {
    static let currentSession = IGSessionManager()
    var ref:FIRDatabaseReference!
    
    private init() {}
    
    func setup() {
        self.ref = FIRDatabase.database().reference()
    }
}