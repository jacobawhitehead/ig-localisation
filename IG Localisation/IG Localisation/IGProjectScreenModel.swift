//
//  IGProjectScreenModel.swift
//  IG Localisation
//
//  Created by Jacob Whitehead on 28/07/2016.
//  Copyright © 2016 Level Headed Studio. All rights reserved.
//

import Foundation

class IGProjectScreenModel: Equatable {
    var id = ""
    var title = ""
    var localisationListID = [String]()
    var platform = ""
    
    init(id:String, title:String, localisationListID: [String], platform: String) {
        self.id = id
        self.title = title
        self.localisationListID = localisationListID
        self.platform = platform
    }
    
    func update(id:String, title:String, localisationListID: [String], platform: String) {
        self.id = id
        self.title = title
        self.localisationListID = localisationListID
        self.platform = platform
    }
}

func ==(lhs: IGProjectScreenModel, rhs: IGProjectScreenModel) -> Bool {
    return lhs.id == rhs.id
}
