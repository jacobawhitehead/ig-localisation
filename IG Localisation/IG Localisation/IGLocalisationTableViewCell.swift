//
//  LocalisationTableViewCell.swift
//  IG Localisation
//
//  Created by Jacob Whitehead on 23/07/2016.
//  Copyright © 2016 Level Headed Studio. All rights reserved.
//

import UIKit

class IGLocalisationTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var contextLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.cardView.layer.masksToBounds = false
        let path = UIBezierPath(rect: self.cardView.bounds)
        self.cardView.layer.shadowPath = path.CGPath
        self.cardView.layer.shadowOffset = CGSizeMake(-0.2, 0.2)
        self.cardView.layer.shadowRadius = 1
        self.cardView.layer.shadowOpacity = 0.2
        self.cardView.alpha = 1
    }
    
    override func setHighlighted(highlighted: Bool, animated: Bool) {
        if highlighted {
            self.cardView.backgroundColor = UIColor.lightGrayColor()
        } else {
            self.cardView.backgroundColor = UIColor.whiteColor()
        }
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        //do nothing!
    }
    
    func setupWithLocalisation(localisation: IGLocalisation) {
        self.titleLabel.text = localisation.text
        self.contextLabel.text = localisation.context
    }
    
}
