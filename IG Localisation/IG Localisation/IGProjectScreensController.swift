//
//  IGProjectScreensController.swift
//  IG Localisation
//
//  Created by Jacob Whitehead on 28/07/2016.
//  Copyright © 2016 Level Headed Studio. All rights reserved.
//

import Foundation
import Firebase

protocol IGProjectScreenDelegate {
    func screenControllerDidUpdate() //TODO: Better logic here?
}

class IGProjectScreensController {
    private var allScreens = [IGProjectScreenModel]()
    private var screenIDs = [String]()
    private var screenRef:FIRDatabaseReference!
    
    var delegate:IGProjectScreenDelegate?
    
    var iOSScreens = [IGProjectScreenModel]()
    var androidScreens = [IGProjectScreenModel]()
    var webScreens = [IGProjectScreenModel]()
    
    init(screenIDs:[String]) {
        self.screenIDs = screenIDs
        self.screenRef = FIRDatabase.database().reference().child("screen")
    }
    
    func getScreens() {
        for screenID in screenIDs {
            self.screenRef.child(screenID).observeEventType(.Value, withBlock: { (snapshot:FIRDataSnapshot) in
                self.addScreenWithSnapshot(snapshot)
            })
        }
    }
    
    private func addScreenWithSnapshot(snapshot:FIRDataSnapshot) {
        let id = snapshot.key
        let title = snapshot.value!["title"] as? String ?? ""
        let localisationIDs = self.localisationListForSnapshot(snapshot)
        let platform = snapshot.value!["platform"] as? String ?? ""
        let screen = IGProjectScreenModel(id: id, title: title, localisationListID: localisationIDs, platform: platform)
        self.updateArray(screen)
    }
    
    private func localisationListForSnapshot(snapshot:FIRDataSnapshot) -> [String] {
        var localisationIDs = [String]()
        var dict = [String:Bool]()
        dict = (snapshot.value!["localisation-list"] as? [String:Bool])!
        
        for (key, _) in dict {
            localisationIDs.append(key)
        }
        
        return localisationIDs
    }
    
    private func updateArray(screen:IGProjectScreenModel) {
        if screen.platform == kPlatformIOS {
            if let index = self.iOSScreens.indexOf(screen) {
                self.iOSScreens.insert(screen, atIndex: index)
            } else {
                self.iOSScreens.append(screen)
            }
        } else if screen.platform == kPlatformAndroid {
            if let index = self.androidScreens.indexOf(screen) {
                self.androidScreens.insert(screen, atIndex: index)
            } else {
                self.androidScreens.append(screen)
            }
        } else if screen.platform == kPlatformWeb {
            if let index = self.webScreens.indexOf(screen) {
                self.webScreens.insert(screen, atIndex: index)
            } else {
                self.webScreens.append(screen)
            }
        }
        
        self.delegate?.screenControllerDidUpdate()
    }
}
